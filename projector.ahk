#Include .\lib\nexus.ahk
#Include .\lib\drinkPotion.ahk
#Include .\lib\tome.ahk
#Include .\lib\cursorFunctions.ahk
#Include .\lib\announcement.ahk
#Include .\lib\getPosition.ahk

;;--------------------------------------------------------------------------------------------------------------------------------
;; Installation instructions:
;; 0. Be a pro. There will be no further explaining that
;; 1. Screenshoot your ign and save as name.bmp
;; 2. Measure your health Bar Length in pixels and assign the value to the healthBarLength variable (edit this script line 28)
;; 3. Edit the browser value if you are not using Firefox (line 27)
;; 3. Read the fu*** script to know what and how
;;--------------------------------------------------------------------------------------------------------------------------------

#SingleInstance force ; only one instance (reload script if already launched) 
;;Process, Priority, , High ; script process priority = HIGH
SetTitleMatchMode 2

;;--------------------------------------------------------------------------------------------------------------------------------
;; User Preferences
hpTomeTrigger := 75 ;; % to hit space after
hpHealTrigger := 60 ;; amount as percent. For example hpHealTrigger := 35 means to use health potion when hp is below 35%
hpExitTrigger := 35 ;; amount as percent. For example hpExitTrigger := 25 means to exit when hp reaches below 25%
manaHealTrigger := 40 ;; amount as percent. For example manaHealTrigger := 35 means to use mana potion when mana is below 35%
myFriend := "Siohan" ;; your friend name -for teleport purpose)
mustDrinkMana := true ;; if setted to true, mana potions are automatically drunk 
isPriest := false ;; must be setted to true if playing a priest
;;--------------------------------------------------------------------------------------------------------------------------------

;;--------------------------------------------------------------------------------------------------------------------------------
;; User variables
MyBrowser := "Mozilla Firefox" ;;"Internet Explorer" ;;"Mozilla Firefox" ;; name of your web browser as displayed in Title Bar after opening any web page
healthBarLength := 176 ;; Amount in pixels of length of hp Bar (depends mostly on your zoom level, also on browser, you can take a sc and measure it with ms paint)
Swf := "http://www.realmofthemadgod.com/AGCLoader1340144946.swf"

;; List of known Images
;;-------------------------------------------------------
;; health potions
img_healthPot := A_WorkingDir . "\images\healthPot.png"
img_healthPot2 := A_WorkingDir . "\images\healthPot2.bmp"
img_healthPot3 := A_WorkingDir . "\images\healthPot2.bmp"
;;-------------------------------------------------------

;;-------------------------------------------------------
;; empty slots (we don't use slots 1&2 because they can be hidden by different windows)
img_emptySlot2 := A_WorkingDir . "\images\slot2.bmp"
img_emptySlot3 := A_WorkingDir . "\images\slot3.bmp"
img_emptySlot4 := A_WorkingDir . "\images\slot4.bmp"
img_emptySlot6 := A_WorkingDir . "\images\slot6.bmp"
img_emptySlot7 := A_WorkingDir . "\images\slot7.bmp"
img_emptySlot8 := A_WorkingDir . "\images\slot8.bmp"
;;-------------------------------------------------------

img_manaPot := A_WorkingDir . "\images\manaPot.bmp"

img_myIgn := A_WorkingDir . "\images\name.bmp" ;; take a screenshoot of your ign in game as seen in example and replace it. See img_myIgn for the details on what to replace
img_hpBarPixel := A_WorkingDir . "\images\hpBarPixel.png"
img_manaBarPixel := A_WorkingDir . "\images\manaBarPixel.jpg" ;; A_WorkingDir . "\images\manaBarPixel.png"
;;--------------------------------------------------------------------------------------------------------------------------------

;;--------------------------------------------------------------------------------------------------------------------------------
;; Declarations of variables
myIgnXPos := 0
myIgnYPox := 0
healthBarStartXPos := 0
healthBarStartYPos := 0
manaBarStartXPos := 0
manaBarStartYPos := 0
healthBarStatusAsPercent := 0
manaBarStatusAsPercent := 0
slot := ""
itemlooted := false
alwaysLoop := true
logFile = %A_ScriptDir%\hpStatusLog.txt
updateTime := 0
status := true
;;--------------------------------------------------------------------------------------------------------------------------------

;;--------------------------------------------------------------------------------------------------------------------------------
;; Functions

getMyIgnPosition() ;; return the IGN position (never exist if not found!)
{
	global myIgnXPos
	global myIgnYPos
	global img_myIgn
	global windowSizeX
	global windowSizeY
 
	startPlaceExists := 0
	While (startPlaceExists = 0)
	{
		ImageSearch, myszX, myszY, 1, 1, windowSizeX, windowSizeY, *15 %img_myIgn% ;; get the name image position
		if (ErrorLevel = 0) 
		{
		  ;; if found, get the position and exit
		  myIgnXPos := myszX
		  myIgnYPos := myszY
		  startPlaceExists := 1
		  ;;MsgBox, "name found at position: %myszX% %myszY%" ;; debug message (uncomment if needed)
		}
		else
		{
		  ;; otherwise do nothing (or send message)
		  MsgBox, "name not found!" ;; debug message (uncomment if needed)	
		}
		
	    Sleep, 100	
	}   
}

getBasicHealthBarPosition() ;; return the Health bar position (never exist if not found!)
{
    global healthBarStartXPos
    global healthBarStartYPos
    global img_hpBarPixel
    global myIgnXPos
    global myIgnYPos
   
	Sleep, 250 ;; let the window load really really properly :P Could use WinWaitActive, but come on, I won't make the code THAT pretty
    
	startPlaceExists := 0	
	While (startPlaceExists = 0)
    {
		;; search the position regarding the ING position
		ImageSearch, myszX, myszY, myIgnXPos - 25, myIgnYPos + 5, myIgnXPos + 15 , myIgnYPos + 244, *15 %img_hpBarPixel%
		
		if (ErrorLevel = 0) 
		{
			;; if found, get the position and exit
			healthBarStartXPos := myszX
			healthBarStartYPos := myszY
			;;MouseMove, myszX, myszY ;; DEBUG line
			;;MsgBox, "health bar found at position: %myszX% %myszY%" ;; DEBUG line
			startPlaceExists := 1
		}  
		else
		{
		  ;; otherwise do nothing (or send message)
		  MsgBox, "hp bar not found!" ;; debug message (uncomment if needed)	
		}
		
		Sleep, 100		
	}		
}

getBasicManaBarPosition() ;; return the Mana bar position (never exist if not found!)
{
    global manaBarStartXPos
    global manaBarStartYPos
    global img_manaBarPixel
    global myIgnXPos
    global myIgnYPos
    
	startPlaceExists := 0	
	While (startPlaceExists = 0)
    {
		;; search the position regarding the ING position
		ImageSearch, myszX, myszY, myIgnXPos - 25, myIgnYPos + 5, myIgnXPos + 15 , myIgnYPos + 244, *15 %img_manaBarPixel%
		
		if (ErrorLevel = 0) 
		{
			;; if found, get the position and exit
			manaBarStartXPos := myszX
			manaBarStartYPos := myszY
			;;MouseMove, myszX, myszY ;; DEBUG line
			;;MsgBox, "mana bar found at position: %myszX% %myszY%" ;; DEBUG line
			startPlaceExists := 1
		}  
		else if  (ErrorLevel = 1) 
		{
		  ;; otherwise do nothing (or send message)
		  MsgBox, "mana bar not found!" ;; debug message (uncomment if needed)	
		}
		else
		{
			MsgBox, "big error"
		}
		
		Sleep, 100		
	}		
}

healOrExitIfNecessary() 
{
    global healthBarStatusAsPercent
    global hpExitTrigger
    global hpHealTrigger
	global hpTomeTrigger
	global isPriest
	
	getHealthBarStatusAsPercent() ;; initialize the hp bar percent value (value contained in 'healthBarStatusAsPercent' variable)
	;;MsgBox, "health bar as percent is:" %healthBarStatusAsPercent%
	
	if( healthBarStatusAsPercent > 0)
	{
		if (healthBarStatusAsPercent < hpExitTrigger )
		{
			Sleep, 50
			getHealthBarStatusAsPercent()
			if (( healthBarStatusAsPercent < hpExitTrigger ) && ( healthBarStatusAsPercent > 0 )) 
			{
				nexus()
			}
		} 
		else if ( healthBarStatusAsPercent < hpHealTrigger )
		{
			heal()
		} 
		else if (( healthBarStatusAsPercent < hpTomeTrigger ) && isPriest) 
		{
			tome()
		}
	}
}

regenManaIfNecessary()
{
	global manaBarStatusAsPercent
    global manaHealTrigger	
	
	if(mustDrinkMana = false)
	{
		return
	}
	
	getManaBarStatusAsPercent()
	;;MsgBox, "mana bar as percent is:" %manaBarStatusAsPercent%
	
	if (( manaBarStatusAsPercent < manaHealTrigger ) && ( manaBarStatusAsPercent > 1 )) 
	{
		Sleep, 50
		getManaBarStatusAsPercent()	
		if (( manaBarStatusAsPercent < manaHealTrigger ) && ( manaBarStatusAsPercent > 1 )) 
		{
			;;MsgBox, "drink mana... %manaBarStatusAsPercent% / %manaHealTrigger%"
			drinkmana()
		}
	}
}

getHealthBarStatusAsPercent() 
{
   global healthBarStartXPos
   global healthBarStartYPos   
   global healthBarLength
   global healthBarStatusAsPercent
   global img_hpBarPixel
   global logFile
   global updateTime
   
   ;MsgBox, "We getting hp status now" ;; DEBUG line
   notFound := false
   toLoop := true
   minOffset := 1
   maxOffset := healthBarLength
   middleShot := (minOffset + maxOffset) // 2 ;; + (healthBarLength // 4) ;; it favourizes the fact that we are USUALLY healed (and if not we heal after 1 loop so we are healed)
                                    ;; can't create different middleshot - 50-76% is missed then. I dun wanna recode all that
   ;; ---------------------------- A simple Binary Search here ----------------------------------
   while ( toLoop = true )
   {      
	  ImageSearch, myszX, myszY, healthBarStartXPos + middleShot, healthBarStartYPos - 3, healthBarStartXPos + middleShot + 9, healthBarStartYPos + 11, *19 %img_hpBarPixel%
	  if (ErrorLevel = 0) 
	  {
		 if ((minOffset = maxOffset) || ((minOffset + 1) = maxOffset)) {
			toLoop := false
		 }
		 minOffset := (minOffset + maxOffset) // 2
	  } 
	  else if (ErrorLevel = 1) 
	  {
		 if ((minOffset = maxOffset) || ((minOffset + 1) = maxOffset)) 
		 {
			toLoop := false
			notFound := true
		 }
		 maxOffset := (minOffset + maxOffset) // 2
	  }
	  middleShot := (minOffset + maxOffset) // 2
   }
   
	;MsgBox, "we found max offset! it is:" %maxOffset% ;; DEBUG line
	if(notFound = true)
	{
		healthBarStatusAsPercent = -100
	}
	else
	{
		healthBarStatusAsPercent := Floor(( middleShot / healthBarLength ) * 100) + 1
	}
	
	GuiControl,, MyProgress, %healthBarStatusAsPercent%
	GuiControl,, MyTextHp, Hp Level =  %healthBarStatusAsPercent% `%
	if (updateTime = 9) 
	{ ;; we will write every 10th of the checking
		updateTime := 0
		FormatTime, timeString,, mm:ss ;;Gettin all that as table to dump at the END. gettin the time only 1 every 10 cases?
		;;FileAppend, %healthBarStatusAsPercent% %timeString%`r`n, %logFile%
	} 
	else 
	{
		updateTime += 1
		;;FileAppend, %healthBarStatusAsPercent%`r`n, %logFile% ;; really no point in writting SO MUCH into the file
	}
}

getManaBarStatusAsPercent() 
{
	global manaBarStartXPos
	global manaBarStartYPos
	global healthBarLength
	global manaBarStatusAsPercent
	global img_manaBarPixel
	global logFile
	global updateTime
   
   ;;MsgBox, "We getting mana status now (%healthBarLength%)" ;; DEBUG line
   toLoop := true
   notFound := false
   minOffset := 1
   maxOffset := healthBarLength
   middleShot := (minOffset + maxOffset) // 2 
   ;; ---------------------------- A simple Binary Search here ----------------------------------
   while ( toLoop = true )
   {      
      ImageSearch, myszX, myszY, manaBarStartXPos + middleShot, manaBarStartYPos - 3, manaBarStartXPos + middleShot + 9, manaBarStartYPos + 11, *19 %img_manaBarPixel%
      if (ErrorLevel = 0) 
	  {
         if ((minOffset = maxOffset) || ((minOffset + 1) = maxOffset)) {
            toLoop := false
         }
         minOffset := (minOffset + maxOffset) // 2
      } 
	  else if (ErrorLevel = 1) 
	  {
         if ((minOffset = maxOffset) || ((minOffset + 1) = maxOffset)) 
		 {
            toLoop := false
			notFound := true
         }
         maxOffset := (minOffset + maxOffset) // 2
      }
      middleShot := (minOffset + maxOffset) // 2
   }
   
   ;;MsgBox, "we found max offset! it is:" %maxOffset% ;; DEBUG line
   
   	if(notFound = true)
	{
		manaBarStatusAsPercent = -100
	}
	else
	{
		manaBarStatusAsPercent := Floor(( middleShot / healthBarLength ) * 100) + 1
	}
   
	;;MsgBox, "mana status percent: %manaBarStatusAsPercent%"
	GuiControl,, MyProgressMana, %manaBarStatusAsPercent%
	GuiControl,, MyTextMana, MP Level =  %manaBarStatusAsPercent% `%
	if (updateTime = 9) 
	{ ;; we will write every 10th of the checking
		updateTime := 0
		FormatTime, timeString,, mm:ss ;;Gettin all that as table to dump at the END. gettin the time only 1 every 10 cases?
		;;FileAppend, %manaBarStatusAsPercent% %timeString%`r`n, %logFile%
	} 
	else 
	{
		updateTime += 1
		;;FileAppend, %manaBarStatusAsPercent%`r`n, %logFile% ;; really no point in writting SO MUCH into the file
	}
}

heal() 
{
	global myIgnXPos
	global myIgnYPos
	global img_healthPot
	global img_healthPot2	
	global img_healthPot3
	global healthBarStatusAsPercent
	global logFile
	
	;;FileAppend, we are initiating heal because hp is %healthBarStatusAsPercent% , %logFile%
	
	;; get the current position
	MouseGetPos, localMouseXPos, localMouseYPos
	
	;; looking for a health pot in the inventory
    ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %img_healthPot%
    if (ErrorLevel = 0) 
	{
		;; if found, shift right clik on it
		;;MsgBox, "we found healing potion" ;; debug message (uncomment if needed)
		getAndDrinkPotion(localMouseXPos, localMouseYPos, myszX, myszY)
    } 
	else 
	{
		;; looking for a second health pot in the inventory
		ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %img_healthPot2%
	    if (ErrorLevel = 0) 
		{
			getAndDrinkPotion(localMouseXPos, localMouseYPos, myszX, myszY)
		}
		else
		{
			;; looking for a third health pot in the inventory
			ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %img_healthPot3%
			if (ErrorLevel = 0) 
			{
				getAndDrinkPotion(localMouseXPos, localMouseYPos, myszX, myszY)
			}
			else	
			{			
				;; otherwise send a message... you are in the shit
				GuiControl,, MyTextHp, HP Level:  %healthBarStatusAsPercent% `%  Too low. No HP POTION available
				;;FileAppend, no hp potion cant heal at all`r`n, %logFile%
			}
		}     
	}
}

drinkmana() 
{
	global myIgnXPos
	global myIgnYPos
	global img_manaPot
	global manaBarStatusAsPercent
	global logFile
   
	;;MsgBox, "we will drink mana"
	;;FileAppend, we are initiating mana heal because mana is %manaBarStatusAsPercent% , %logFile%
	
	;; get the current position
	MouseGetPos, localMouseXPos, localMouseYPos
	
	;; looking for a mana pot in the inventory
    ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %img_manaPot%
    if (ErrorLevel = 0) 
	{
		;; if found, shift right clik on it
		;;MsgBox, "we found mana potion" ;; debug message (uncomment if needed)	
		getAndDrinkPotion(localMouseXPos, localMouseYPos, myszX, myszY)	
    } 
	else 
	{
		;; otherwise send a message... you are in the shit
      GuiControl,, MyTextMana, Mana Status:    %manaBarStatusAsPercent%  Too low. No Mana POTION available
      ;;FileAppend, no mana potion, cant heal at all`r`n, %logFile%
	}
}

initiate() ;; main method, initialize all monitorings
{
	global alwaysLoop
	global windowSizeX
	global windowSizeY
	global MyBrowser
	global logFile
	global status
   
	;;MsgBox, "initialization" ;; DEBUG Line
	SetTitleMatchMode, RegEx   ;RegEx = sets title matching mode to regular expressions ( 1 = exact 2 = anywhere inside )
   
	WinActivate, Adobe Flash Player 10
	;; activate the brower window
	;;WinActivate, .* %MyBrowser% ;window's name ends with the browser name ( %MyBrowser% )
   
	;; get the window size (for upcoming search)
	WinGetPos, , , windowSizeX, windowSizeY , A
   
	;; get the IGN position
	getMyIgnPosition()
   
	;; get the hp bar position
	getBasicHealthBarPosition()
	
	;; get the mana bar position
	getBasicManaBarPosition()
	  
	;;FileAppend, we are initiating the script now`r`n, %logFile%
	;;MsgBox, "we will start endless loop now!" ;; DEBUG line
	;; start the monitoring (each 10 ms)
	count := 0
	while (alwaysLoop = true) 
	{
		if(status = true)
		{
			healOrExitIfNecessary()
			regenManaIfNecessary()
			;;if(count = 5)
			;;{
			;;	regenManaIfNecessary()
			;;	count := 0
			;;}
			;; ninja loot activated
			getObject()
			Sleep, 20
			count += 1
		}
		else
		{
			GuiControl,, MyTextHp, STOPPED!!!
		}
	}
	return
}

getObject() ;; move a selected object on ground to the inventory (if an empty slot is found)
{
	global mousePosX
	global mousePosY	
	
	GetKeyState, state, C
	if state = D
	{		
		;; get the current position (where the item should be)
		MouseGetPos, mousePosX, mousePosY	

		;; go to the first slot		
		getObjectAtPosition(1200, 640)	
		
		;; go to the second slot	
		;;getObjectAtPosition(1245, 640)	
	
		;; go to the third slot	
		;;getObjectAtPosition(1290, 640)	

		MouseMove, mousePosX, mousePosY				
	}
}

getObjectAtPosition(x, y)
{
	global img_emptySlot2
	global img_emptySlot3
	global img_emptySlot4
	global img_emptySlot6
	global img_emptySlot7
	global img_emptySlot8
	global slot
	global itemlooted
		
	itemlooted := false
	
	;; go to the position
	MouseMove, x, y
	
	slot := img_emptySlot2
	lookingForSlot()
	
	slot := img_emptySlot3
	lookingForSlot()	
	
	slot := img_emptySlot4
	lookingForSlot()	
	
	slot := img_emptySlot6
	lookingForSlot()
	
	slot := img_emptySlot7
	lookingForSlot()	
	
	slot := img_emptySlot8
	lookingForSlot()
	
	Sleep, 10
}

lookingForSlot()
{
	global slot
	global mousePosX
	global mousePosY
	global myIgnXPos
	global myIgnYPos	
	global itemlooted
	
	if(itemlooted = true)
	{
		return
	}
	
	;; looking for an empty slot in the inventory
	ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %slot%
	;;MsgBox, "search empty slot : %slot%" ;; debug message (uncomment if needed)
	if (ErrorLevel = 0) 
	{
		;; if found, shift right clik on it
		;; get the slot position
		nearestEmptySlotXPos := myszX + 5
		nearestEmptySlotYPos := myszY + 5
		;;MsgBox, "IGN pos: %myIgnXPos% %myIgnYPos% \r\n nearest position: %nearestEmptySlotXPos% %nearestEmptySlotXPos%" ;; debug message (uncomment if needed)
		
		;; press the left mouse click
		Click down		
		Sleep, 10
		
		;; move the mouse over the empty slot
		MouseMove, nearestEmptySlotXPos, nearestEmptySlotYPos
		
		;; click on the pot
		Click up	
		Sleep, 10
		
		;; move the mouse cursor to its initial position
		;;MouseMove, mousePosX, mousePosY
		
		itemlooted := true
		
		;;FileAppend, . We get an item from the floor correctly`r`n, %logFile%
	} 	
	else if(ErrorLevel = 2)
	{
		;;MsgBox, "image file not  found" ;; debug message (uncomment if needed)
	}
	else 
	{
		;; otherwise send a message...
		GuiControl,, MyTextHp, Can't get the item, No available slot
		;;FileAppend, no hp potion cant heal at all`r`n, %logFile%
	}		
}

updateManaDrink()  ;; updates the mana drink option the GUI one
{
	global mustDrinkMana
	
	if(mustDrinkMana = false)
	{
		mustDrinkMana := true
	}
	else
	{
		mustDrinkMana := false
	}
}

updatePriest()  ;; updates the priest skill with the GUI one
{
	global isPriest
	
	if(isPriest = false)
	{
		isPriest := true
	}
	else
	{
		isPriest := false
	}
}

updateStatus()  ;; change the current status
{
	global status
	
	if(status = false)
	{
		status := true
	}
	else
	{
		status := false
	}
}

updateTeleport(newValue) ;; updates the teleport friend name with the GUI one
{
	global myFriend
	
	myFriend := newValue
	return
}

updateTriggers(nexus, hp, tome, mana) ;; updates all trigger values with the new GUI ones
{
	global hpExitTrigger
	global hpHealTrigger
	global hpTomeTrigger
	global manaHealTrigger

	hpExitTrigger := nexus
	hpHealTrigger := hp
	hpTomeTrigger := tome
	manaHealTrigger := mana
}

;;--------------------------------------------------------------------------------------------------------------------------------

;;--------------------------------------------------------------------------------------------------------------------------------
;; Main part of the script
;;--------------------------------------------------------------------------------------------------------------------------------

FileDelete, %logFile%
Gui,+AlwaysOnTop -SysMenu
Gui, Add, Progress, vMyProgress x5 35 w180 h20 cRed, 100
Gui, Add, Text, vMyTextHp x5 y30 w180, HP Status
Gui, Add, Progress, vMyProgressMana x5 y55 w180 h20, 100
Gui, Add, Text, vMyTextMana x5 y80 w180, MP Status
Gui, Add, Checkbox, x5 y105 w70 gCheckBoxMana vCheckBoxStatus Checked, Mana
Gui, Add, Checkbox, x75 y105 w70 gCheckBoxPriest vCheckBoxStatus2, Priest
Gui, Font, underline
Gui, Add, Text, x5 y130 w70, Triggers:
Gui, Font, norm
Gui, Add, Text, x5 y158 w30, Nexus:
Gui, Add, Edit, x40 y155 w30 vNexusValue gTriggersChanged, %hpExitTrigger%
Gui, Add, Text, x75 y158 w30, HP:
Gui, Add, Edit, x105 y155 w30 vHpValue gTriggersChanged, %hpHealTrigger%
Gui, Add, Text, x5 y183 w30, Tome:
Gui, Add, Edit, x40 y180 w30 vTomeValue gTriggersChanged, %hpTomeTrigger%
Gui, Add, Text, x75 y183 w30, Mana:
Gui, Add, Edit, x105 y180 w30 vManaValue gTriggersChanged, %manaHealTrigger%
Gui, Add, Text, x5 y213 w70, Teleport (F3):
Gui, Add, Edit, x75 y210 w100 vTeleportValue gTeleportChanged, %myFriend%
Gui, Add, Button, x70 y235 w50 gChangeStatus, ON/OFF
Gui, Show, x50 y455 h260 w190, RotMG Helper

initiate() ;; run the main loop

;;--------------------------------------------------------------------------------------------------------------------------------
;; GUI CallBacks
;;--------------------------------------------------------------------------------------------------------------------------------
CheckBoxMana:
	Gui, Submit, NoHide
	updateManaDrink()
	return
	
CheckBoxPriest:
	Gui, Submit, NoHide
	updatePriest()
	return
	
TeleportChanged:
	Gui, Submit, NoHide
	updateTeleport(TeleportValue)
	return	

TriggersChanged:
	Gui, Submit, NoHide
	updateTriggers(NexusValue, HpValue, TomeValue, ManaValue)
	return	

ChangeStatus:
	updateStatus()
	return
;;--------------------------------------------------------------------------------------------------------------------------------

;;--------------------------------------------------------------------------------------------------------------------------------
;; Shortcuts
;;--------------------------------------------------------------------------------------------------------------------------------
#IfWinActive, Adobe Flash Player 10 ;#IfWinActive, Realm of the Mad God

Esc::
	ExitApp
	
F1::
	updateCursorAnimation() ;; Activate or Desactivate a more visible cursor
	return
	
F2::
	announcement()
	
F3:: ;; teleport to the defined friend (myFriend variable)
	SendInput, {Enter}/teleport %myFriend%{Enter}
	return
	
;; debug function	
F4:: ;; debug message
	if(status = true)
	{
		MsgBox, status activated!
	}
	return

F5::
	getPosition()
	
g:: ;; exit to nexus
	if(status = true)
	{
		nexus()
		Sleep, 10000
	}
	else
	{
		SendInput, g
	}
	return

c:: ;; pick up the first object on the ground
	if(status = true)
	{
		getobject()
	}
	else
	{
		SendInput, c
	}	
	return
	
r::  ;; ask for heal
	if(status = true)
	{
		SendInput, {Enter}HP plz{Enter}
	}
	else
	{
		SendInput, r
	}	
	return
	
t::	;; thanks
	if(status = true)
	{
		SendInput, {Enter}Thx{Enter}	
	}
	else
	{
		SendInput, t
	}	
	return
	
RButton::	;; disable the shift-click
	if(status = true)
	{
		Send, {SHIFTDOWN}
		Click
		Sleep, 10  ;;30
		Send, {SHIFTUP}
	}
	else
	{
		Send, {RBUTTON}
	}
	Return