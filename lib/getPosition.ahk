;;---------------------------------
;; returns the current mouse location in a message box
;;---------------------------------
getPosition()
{
	;; get the current position
	MouseGetPos, mousePosX, mousePosY	
	
	MsgBox, pos x = %mousePosX%, y = %mousePosY%		
}