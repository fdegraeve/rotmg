;;-----------------------------------------------------------------
;; Activate or Desactivate a more visible cursor (located in in images\cursor.ani)
;;-----------------------------------------------------------------
updateCursorAnimation()
{	
	static CurToggle := 0
	if CurToggle = 0
	{
		SetSystemCursor()
		CurToggle = 1
		Return
	}
	else
	{
		RestoreCursors()
		CurToggle = 0
		Return
	}
}

;;-----------------------------------------------------------------
;; replaces the current cursor by the cursor in images\cursor.ani
;;-----------------------------------------------------------------
SetSystemCursor()
{
	Cursor = %A_ScriptDir%\images\cursor.ani
	CursorHandle := DllCall( "LoadCursorFromFile", Str,Cursor )
	Cursors = 32512,32513,32514,32515,32516,32640,32641,32642,32643,32644,32645,32646,32648,32649,32650,32651
	Loop, Parse, Cursors, `,
	{
		DllCall( "SetSystemCursor", Uint,CursorHandle, Int,A_Loopfield )
	}
}

;;-----------------------------------------------------------------
;; restores the default system cursor animation
;;-----------------------------------------------------------------
RestoreCursors()
{
	SPI_SETCURSORS := 0x57
	DllCall( "SystemParametersInfo", UInt,SPI_SETCURSORS, UInt,0, UInt,0, UInt,0 )
}