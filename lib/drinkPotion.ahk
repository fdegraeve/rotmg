;;---------------------------------
;; drink a potion which is located in (posX, posY) and 
;; move back the mouse to the initial position (localMouseXPos, localMouseYPos)
;;---------------------------------
drinkPotion(localMouseXPos, localMouseYPos, posX, posY)
{
	Sleep, 10  ;;30
	;; press the shift key
	Send, {Shift Down} ;; in case it doesn't work for you, you can use Send, (LShift Down} and then {LShift Up}
	
	Sleep, 10 ;;90
	;; move the mouse over the pot
	MouseMove, posX, posY
	
	;; click on the pot
	MouseClick, Left, posX, posY
	Sleep, 10  ;;50
	
	;; unpress the shift key
	Send, {Shift Up}
	Sleep, 10  ;;90
	
	;; move the mouse cursor to its initial position
	MouseMove, localMouseXPos, localMouseYPos
}

getAndDrinkPotion(localMouseXPos, localMouseYPos, posX, posY)
{
	;; get the pot position
	nearestHpPotXPos := posX + 5
	nearestHpPotYPos := posY + 5	
	drinkPotion(localMouseXPos, localMouseYPos, nearestHpPotXPos, nearestHpPotYPos)		
	return	
}