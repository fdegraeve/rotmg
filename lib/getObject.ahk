;;-----------------------------------------------------------------
;; Global variables
;;-----------------------------------------------------------------
;; empty slots (we don't use slots 1&2 because they can be hidden by different windows)
img_emptySlot2 := A_WorkingDir . "\images\slot2.bmp"
img_emptySlot3 := A_WorkingDir . "\images\slot3.bmp"
img_emptySlot4 := A_WorkingDir . "\images\slot4.bmp"
img_emptySlot6 := A_WorkingDir . "\images\slot6.bmp"
img_emptySlot7 := A_WorkingDir . "\images\slot7.bmp"
img_emptySlot8 := A_WorkingDir . "\images\slot8.bmp"
slot := ""
itemlooted := false

;;-----------------------------------------------------------------
;; move a selected object on ground to the inventory (if an empty slot is found)
;;-----------------------------------------------------------------
getObject()
{	
	GetKeyState, state, C
	
	if state = D
	{		
		;; get the current position (where the item should be)
		MouseGetPos, mousePosX, mousePosY	

		;; go to the first slot		
		getObjectAtPosition(1200, 640)	
		
		;; go to the second slot	
		;;getObjectAtPosition(1245, 640)	
	
		;; go to the third slot	
		;;getObjectAtPosition(1290, 640)	

		;; get back to the initial position
		MouseMove, mousePosX, mousePosY				
	}
}

getObjectAtPosition(x, y)
{
	global img_emptySlot2
	global img_emptySlot3
	global img_emptySlot4
	global img_emptySlot6
	global img_emptySlot7
	global img_emptySlot8
	global slot
	global itemlooted
		
	itemlooted := false
	
	;; go to the position
	MouseMove, x, y
	
	slot := img_emptySlot2
	lookingForSlot()
	
	slot := img_emptySlot3
	lookingForSlot()	
	
	slot := img_emptySlot4
	lookingForSlot()	
	
	slot := img_emptySlot6
	lookingForSlot()
	
	slot := img_emptySlot7
	lookingForSlot()	
	
	slot := img_emptySlot8
	lookingForSlot()
	
	Sleep, 10
}

lookingForSlot()
{
	global slot
	global myIgnXPos
	global myIgnYPos	
	global itemlooted
	
	if(itemlooted = true)
	{
		return
	}
	
	;; looking for an empty slot in the inventory
	ImageSearch, myszX, myszY, myIgnXPos - 85, myIgnYPos, myIgnXPos + 224, myIgnYPos + 520, *20 %slot%
	;;MsgBox, "search empty slot : %slot%" ;; debug message (uncomment if needed)
	if (ErrorLevel = 0) 
	{
		;; if found, shift right clik on it
		;; get the slot position
		nearestEmptySlotXPos := myszX + 5
		nearestEmptySlotYPos := myszY + 5
		;;MsgBox, "IGN pos: %myIgnXPos% %myIgnYPos% \r\n nearest position: %nearestEmptySlotXPos% %nearestEmptySlotXPos%" ;; debug message (uncomment if needed)
		
		;; press the left mouse click
		Click down		
		Sleep, 10
		
		;; move the mouse over the empty slot
		MouseMove, nearestEmptySlotXPos, nearestEmptySlotYPos
		
		;; click on the pot
		Click up	
		Sleep, 10
		
		itemlooted := true
		
		;;FileAppend, . We get an item from the floor correctly`r`n, %logFile%
	} 	
	else if(ErrorLevel = 2)
	{
		;;MsgBox, "image file not  found" ;; debug message (uncomment if needed)
	}
	else 
	{
		;; otherwise send a message...
		GuiControl,, MyTextHp, Can't get the item, No available slot
		;;FileAppend, no hp potion cant heal at all`r`n, %logFile%
	}		
}